package ioc.dam.m9.uf1.eac3.b2;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Usuari
 */

import com.jcraft.jsch.Channel;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.UserInfo;
import static java.lang.System.in;
 
public class ClientSSH {
    private static final String user = "tbigorda";
    private static final String host = "127.0.0.1";
    private static final Integer port = 22;
    private static final String pass = "1234";
 
    public static void main(String[] args) throws Exception{
        System.out.println("----- INICI ------");
        JSch jsch = new JSch();
        Session session = jsch.getSession(user, host, port);
        UserInfo ui = new CaracteristiquesUsuari(pass, null);
        session.setUserInfo(ui);
        session.setPassword(pass);
        session.connect();
        
        
        ChannelExec channelExec = (ChannelExec)session.openChannel("exec");
        String command = "cmd /c cd C dir ";
        channelExec.setCommand(command);
        InputStream in = channelExec.getInputStream();
        
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        String valor = reader.readLine();
        while(valor != null){
            System.out.println(valor);
        }
        
        channelExec.disconnect();
        session.disconnect();
       
        System.out.println("------ FINAL ------");
    }
}