/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ioc.dam.m9.uf1.eac3.b1;

/**
 *
 * @author Usuari
 */

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.KeyStore;
import java.util.Scanner;

import javax.net.ServerSocketFactory;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;

public class FitxerSegurServidor extends Thread {
 private Socket socket;

 public FitxerSegurServidor(Socket socket) {
  this.socket = socket;
 }

 private static String SERVIDOR_CLAU = "C:\\Program Files\\Java\\jdk1.8.0_221\\bin\\server_ks";
 private static String SERVIDOR_CLAU_PASSWORD = "123123";

 public static void main(String[] args) throws Exception {
         
  System.out.println("Esperant connexions");
   
    System.setProperty("javax.net.ssl.trustStore", SERVIDOR_CLAU);
    SSLContext context = SSLContext.getInstance("TLS");

    KeyStore ks = KeyStore.getInstance("JKS");
    ks.load(new FileInputStream(SERVIDOR_CLAU), null);
    KeyManagerFactory kf = KeyManagerFactory.getInstance("SunX509");
    kf.init(ks, SERVIDOR_CLAU_PASSWORD.toCharArray());

    context.init(kf.getKeyManagers(), null, null);
    
    ServerSocketFactory factory = context.getServerSocketFactory();
    ServerSocket  _socket= factory.createServerSocket(8443);
    
  ((SSLServerSocket) _socket).setNeedClientAuth(false);

  while (true) {
      
    //Obtenim un client

    SSLSocket newSocket = (SSLSocket) _socket.accept();

    //Opté canal d'entrada

     InputStream is = newSocket.getInputStream();       

     BufferedReader br = new BufferedReader(new InputStreamReader(is));

     String linea = br.readLine();        

     while (linea != null) {          
        System.out.println(linea.toUpperCase());          
        linea = br.readLine();        
     }
     
     newSocket.close();      
   
    }
 }
}