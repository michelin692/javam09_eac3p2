/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ioc.dam.m9.uf1.eac3.b1;

/**
 *
 * @author Usuari
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.security.KeyStore;
import java.util.Scanner;
import javax.net.SocketFactory;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

public class FitxerClientSegur {
 private static String CLAU_CLIENT = "C:\\Program Files\\Java\\jdk1.8.0_221\\bin\\client_ks";
 private static String CLAU_CLIENT_PASSWORD = "456456";
 
 public static void main(String[] args) throws Exception {
     
  //Estableix el magatzem de claus a utilitzar per validar el certificat del servidor
  System.setProperty("javax.net.ssl.trustStore", CLAU_CLIENT);
  System.setProperty("javax.net.debug", "ssl,handshake");
 
  

  //IMPLEMENTAR
  FitxerClientSegur clientSegur = new FitxerClientSegur();
  Socket sClient = clientSegur.clientAmbCert();
  
 
    Scanner reader = new Scanner (System.in);
    PrintStream writer = new PrintStream(sClient.getOutputStream());
    System.out.println("Deixa una línia en blanc per acabar:");
    String text = reader.nextLine();
     while (!text.equals("")) {
       writer.println(text);
       writer.flush();
       text = reader.nextLine();
     }
    writer.println("<<FI>>");
    writer.flush();
    sClient.close();
  
 }

 private Socket clientSenseCert() throws Exception {
  SocketFactory sf = SSLSocketFactory.getDefault();
  Socket s = sf.createSocket("localhost", 8443);
  return s;
 }

 private Socket clientAmbCert() throws Exception {
  SSLContext context = SSLContext.getInstance("TLS");
  KeyStore ks = KeyStore.getInstance("JKS");
  
  ks.load(new FileInputStream(CLAU_CLIENT), null);
  KeyManagerFactory kf = KeyManagerFactory.getInstance("SunX509");
  kf.init(ks, CLAU_CLIENT_PASSWORD.toCharArray());
  context.init(kf.getKeyManagers(), null, null);
  
  SocketFactory factory = context.getSocketFactory();
  Socket s = factory.createSocket("localhost", 8443);
  return s;
 }
}